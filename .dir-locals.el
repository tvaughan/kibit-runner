((nil . ((eval . (let ((current-dir (file-name-directory
                                     (let ((d (dir-locals-find-file "./")))
                                       (if (stringp d) d (car d))))))
                   (container-tramp-add-method
                    "kibit-runner"
                    :container-executable (concat current-dir "podman-wrapper")))))))
